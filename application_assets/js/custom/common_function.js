/**
 * Created with JetBrains PhpStorm.
 * User: xgen-chamika
 * Date: 7/13/15
 * Time: 3:32 PM
 * To change this template use File | Settings | File Templates.
 */
var base_url = js_base_url;
var site_url = js_site_url;

//alert(site_url);

jQuery.ajaxSetup({async: false});

$(document).keypress(function(e) {
    if(e.which == 13) {
        var keyFunction = $('#keyEnter').val();

        window[keyFunction]();

    }
});



$(function () {

    $('table').footable().bind('footable_filtering', function (e) {
        var selected = $('.filter-status').find(':selected').text();
        if (selected && selected.length > 0) {
            e.filter += (e.filter && e.filter.length > 0) ? ' ' + selected : selected;
            e.clear = !e.filter;

        }
    });

    $('.clear-filter').click(function (e) {
        e.preventDefault();
        $('.filter-status').val('');
        $('table.viewtbl').trigger('footable_clear_filter');
    });

    $('.filter-status').change(function (e) {
        e.preventDefault();
        $('table.viewtbl').trigger('footable_filter', {filter: $('#filter').val()});
    });
});



function imageuploade(imageid,path,tag, file ,maxid, progressid, errorid, viewid, dbidimg, thumb, thwidth, thheight, resize, width, height){

    var error = false;
    var errormsg = '';

    var path = $('#'+path).val();
    var tag = $('#'+tag).val();
    var file_data = $('#'+imageid).prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data)
    form_data.append('path', path)
    form_data.append('tag', tag)
    form_data.append('thumb', thumb)
    form_data.append('thwidth', thwidth)
    form_data.append('thheight', thheight)
    form_data.append('resize', resize)
    form_data.append('width', width)
    form_data.append('height', height)

    $('.progress').show();
//Standards base browsers
    var name = file.name;
    var size = parseInt(file.size / 1024);
    var type = file.type;

    var maxsize = parseInt($('#'+maxid).val()) *1000;

    if(size > maxsize){
        error = true;
        errormsg=' Not Allow Size : '+ size+'KB | Size Allowed: ' +maxsize +'KB';
    }
    if(type != 'image/jpeg' && type != 'image/png' && type != 'image/gif'){
        error = true;
        errormsg='Invalid file type: '+ type;
    }

    if(!error){
        $.ajax({

            url: site_url+'upload_controller',
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (msg) {
                // alert(file);
                var part = msg.split("##**");
                var  filesend = part[0];
                var uploaderrormsg =  part[1];
                if(filesend != 'error' ){
                    $('#'+dbidimg).val(filesend);

                    if(!resize){
                        $('#'+viewid).html(' <img src="'+base_url+path+filesend+'"  width="'+width/2+'px" />' );
                    }else{
                        $('#'+viewid).html(' <img src="'+base_url+path+filesend+'"  width="'+width/2+'px" height="'+height/2+'px"/>' );
                    }

                    $('#'+errorid).html('<div class="alert alert-success alert-dark"> '+uploaderrormsg +'<div>');
                }else{
                    $('#'+errorid).html('<div class="alert alert-danger alert-dark"> '+uploaderrormsg +'<div>');

                }

            } ,
            xhrFields: {
                // add listener to XMLHTTPRequest object directly for progress (jquery doesn't have this yet)
                onprogress: function (progress) {
                    // calculate upload progress
                    var percentage = Math.floor((progress.total / progress.totalSize) * 100);

                    $('#'+progressid).width(percentage+'%');
                    // log upload progress to console
                    console.log('progress', percentage);
                    if (percentage === 100) {
                        $('.progress').hide();
                    }
                }
            }
        });




    }else{
        $('#'+errorid).html('<div class="alert alert-danger alert-dark"> '+errormsg
            +'<div>');
    }



}
